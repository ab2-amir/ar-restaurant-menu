using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;


public class Menu : MonoBehaviour
{
    public GameObject[] food;
    List<string> prixList = new List<string>();
    public TextMeshPro prix;
    // public Text ingredients;

    public TextMeshPro ingredients;
    public GameObject IngredientsGameObject;


    void Start() {
        prixList.Add("15");
        prixList.Add("5");
        prixList.Add("12");
        prixList.Add("4");
    }

    // public GameObject GazedPlan;
    

    public void openPizza()
    {
        for (int i=0; i < food.Length; i++)
        {
            food[i].SetActive(false);
        }
        food[0].SetActive(true);
        prix.text = prixList[0] + " $";
        ingredients.text = "- Tomato sauce" + "\n" + "- Fresh mozzarella"+ "\n" + "- Fresh basil leaves "+ "\n" + "- oregano ";
        IngredientsGameObject.SetActive(true);
        // GazedPlan.SetActive(true);
    }

    public void openFrenchFries()
    {
        for (int i=0; i < food.Length; i++)
        {
            food[i].SetActive(false);
        }
        food[1].SetActive(true);
        prix.text = prixList[1] + " $";
        ingredients.text = "- Fresh Potatoes" + "\n" + "- Vegetable Oil" + "\n" + "- A Dash of Salt"+ "\n" + "- ketchup";
        IngredientsGameObject.SetActive(true);
        // GazedPlan.SetActive(true);
    }
    public void openSushi()
    {
        for (int i=0; i < food.Length; i++)
        {
            food[i].SetActive(false);
        }
        food[3].SetActive(true);
        prix.text = prixList[2] + " $";
        ingredients.text = "- Fresh crab meat" + "\n" + "- Avocado slices" + "\n" + "- Cucumber strips"+ "\n" + "- Sushi rice"+ "\n" + "- Sesame seeds"+ "\n" + "- Soy sauce";
        IngredientsGameObject.SetActive(true);
        // GazedPlan.SetActive(true);
    }

    public void openCoffee()
    {
        for (int i=0; i < food.Length; i++)
        {
            food[i].SetActive(false);
        }
        food[2].SetActive(true);
        prix.text = prixList[3] + " $";
        ingredients.text = "- Espresso shots" + "\n" + "- Steamed milk" + "\n" + "- Chocolate syrup";
        IngredientsGameObject.SetActive(true);
        // GazedPlan.SetActive(true);
    }


    public void exitApp()
    {
        Application.Quit();
    }
}
